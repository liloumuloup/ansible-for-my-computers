# Ansible for my computers

Automate installation of brand new computers

## Prerequisites :

* Install Ansible
* Install galaxy role for Gnome Shell

```
ansible-galaxy role install PeterMosmans.customize-gnome
```

## Shell tools :

- Terminator : package
- Git : package
- zsh + zinit : packages
- flatpak : package
- htop : package
- neovim + config : package
- Gitup (https://korben.info/mettez-a-jour-vos-depots-git-en-une-seule-fois.html)

## Other tools :

- Buttercup : https://github.com/buttercup/buttercup-desktop
- Pcloud : https://www.pcloud.com/fr/download-free-online-cloud-file-storage.html
- Firebot : https://firebot.app/
